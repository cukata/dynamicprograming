# DynamicPrograming

# Zadání bylo vytvořeno k předmětu Algoritmy a grafy na FIT ČVUT
Zadání se zaměřuje na procivčení dynamického programování 

A to jsem Vám minule zapomněl říci. Kdovíci mají Vánoce tak rádi, že mají doma ozdobený stromeček po celý rok. V 
hračkářství mají nejen letní Vánoce, ale i podzimní a jarní. Kam se na to hrabe náš zelený mimozemšťan. A v 
neposlední řadě je třeba zmínit, že i systém ulic v Kdovíkově tvoří strom. Nepříliš praktické, ale to tu nikomu 
nevadí.

Co se málo ví, a to i mezi Kdovíky, je, že tato malá vesnička má vlastní tajné služby. Jednotka se jmenuje Grinch 
Resitance Unit (GRU) a jejím hlavním úkolem je, pochopitelně, ochrana Kdovíkova a Vánoc před zlovolnostmi této všemi 
nenáviděné potvory.

Ihned poté, co agenti tajných služeb zachytili první náznaky tohoto, co Grinch chystá, se sešla bezpečností rada 
Kdovíkova. A začala připravovat protiopatření. Jaké všechny kroky připravili, nevíme. O jednom Vám ale něco říci 
mohu, bytostně se Vás totiž týká.

Díky tomu, že je majitelem jediného hračkářství v Kdovíkově, a díky nedávno založené Adventní Evidenci Trhů (AET), 
má starosta úplný přehled o tom, kdo, kde a co nakoupil. A tedy i o tom, co lze pod kterým stromečkem o Vánoční noci 
očekávat.

Jelikož jste se osvědčili při přípravě Nejprodávanějšího zboží, starosta se rozhodl oslovit Vás i pro tuto prácičku. 
Vaším úkolem je najít rozestavení agentů tajné služby v ulicích města tak, aby bylo uchráněno co největší množství 
dárků. Dle prastarých zvyků se dárky vždy objevují pod stromečky umístěnými na křižovatkách a náměstích tak, aby 
Kdovíci mohli okamžitou radost z darů sdílet s ostatními.

Ale pozor! Kdovíci si nesmí agentů v ulicích za žádnou cenu všimnout. Pokud by mezi obyvateli došlo k panice na 
Štědrý den, nemusel by to starosta ustát. Proto dej pozor, aby se v ulicích neobjevovali velké skupinky agentů 
blízko sebe. Agenti jsou považováni za tutéž skupinu, pokud mezi nimi není žádný nehlídaný stromek.

# Rozhraní programu
Vaším úkolem je implementovat funkci solve( TreeProblem ), která spočte, kolik dárků lze uchránit při 
nejlepším možném rozestavění strážců. Instance problému je popsána třídou TreeProblem, která obsahuje položky:

max_group_size: Maximální velikost skupiny strážců. Tato hodnota je 1.

gifts: List určující kolik dárků je pod kterým stromkem. Délka tohoto vektoru také udává celkový počet stromků.

connections: List dvojic udávájící, které stromky jsou na opačných koncích téže ulice, takže pokud jsou oba 
hlídané, náleží jejich strážci do téže skupiny. Připomínáme, že síť ulic Kdovíkova tvoří strom.