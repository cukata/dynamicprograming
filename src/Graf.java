import java.util.ArrayList;

public class Graf
{
	
public ArrayList<ArrayList<Integer>> edges;
public ArrayList<Boolean> visited;
public ArrayList<Integer> helpGuard;
public ArrayList<Integer> helpWithout;

public Graf(){
	 edges = new ArrayList<ArrayList<Integer>>();
	 visited = new ArrayList<Boolean>();
	 helpGuard = new ArrayList<Integer>();
	 helpWithout = new ArrayList<Integer>();
}

	public Graf(Graf graf ){
		edges = new ArrayList<ArrayList<Integer>>();
		edges.addAll(graf.edges);
		visited = new ArrayList<Boolean>();
		visited.addAll(graf.visited);
		helpGuard = new ArrayList<Integer>();
		helpGuard.addAll(graf.helpGuard);
		helpWithout = new ArrayList<Integer>();
		helpWithout.addAll(graf.helpWithout);

	}
    public final void refactorEdges(TreeProblem in) {
		for( int i = 0; i < in.gifts.size(); i++ ){
			visited.add(false);
			edges.add(new ArrayList<Integer>());
			helpGuard.add(-1);
			helpWithout.add(-1);
		}
		for (int i = 0; i < in.connections.size(); i++)
		{
			edges.get((Integer) in.connections.get(i).first).add((Integer) in.connections.get(i).second);
			edges.get((Integer) in.connections.get(i).second).add((Integer) in.connections.get(i).first);
		}
	}

}