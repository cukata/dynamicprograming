import java.util.List;

public class TreeProblem
{
  int max_group_size;
  List<Pair> connections;
  List<Integer> gifts;

  public TreeProblem(int max_group_size, List<Pair> connections, List<Integer> gifts) {
    this.max_group_size = max_group_size;
    this.connections = connections;
    this.gifts = gifts;
  }
}