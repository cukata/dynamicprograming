import java.util.Arrays;

public class Main {

    public static int count(TreeProblem problem, Graf g, int in, boolean guard)
    {
        if (g.edges.get(in).size() == 1 && g.visited.get(g.edges.get(in).get(0)) == true) { // no more neighours
            if (guard == false) {
                g.helpWithout.set(in, 0);
                return 0; //dont have guard
            } else {
                // this tree have guard
                g.helpGuard.set(in, problem.gifts.get(in));
                return problem.gifts.get(in); // return amount of presents under this tree{
            }
        }

        int result;

        if (guard == true) {
            result = 0;
            for (int i = 0; i < g.edges.get(in).size(); i++) {
                if (g.visited.get(g.edges.get(in).get(i)) != true) {
                    g.visited.set(g.edges.get(in).get(i), true);
                    if (g.helpGuard.get(g.edges.get(in).get(i)) != (int)-1) {
                        result += g.helpGuard.get(g.edges.get(in).get(i));
                        g.helpGuard.set(in, g.helpGuard.get(g.edges.get(in).get(i)));
                    } else {
                        int tmp = count(problem, new Graf(g), g.edges.get(in).get(i), false);
                        result += tmp;
                        g.helpWithout.set(g.edges.get(in).get(i), tmp);
                    }
                }
            }
            result += problem.gifts.get(in);
        } else {
            result = 0;
            for (int i = 0; i < g.edges.get(in).size(); i++) {
                if (g.visited.get(g.edges.get(in).get(i)) != true) {
                    g.visited.set(g.edges.get(in).get(i), true);
                    if (g.helpWithout.get(g.edges.get(in).get(i)) != (int)-1 && g.helpGuard.get(g.edges.get(in).get(i)) != (int)-1) {
                        result += Math.max(g.helpWithout.get(g.edges.get(in).get(i)), g.helpGuard.get(g.edges.get(in).get(i)));
                        g.helpWithout.set(in, Math.max(g.helpWithout.get(g.edges.get(in).get(i)), g.helpGuard.get(g.edges.get(in).get(i))));
                    } else if (g.helpWithout.get(g.edges.get(in).get(i)) == (int)-1 && g.helpGuard.get(g.edges.get(in).get(i)) != (int)-1) {
                        int tmp = count(problem, new Graf(g), g.edges.get(in).get(i), false);
                        g.helpWithout.set(g.edges.get(in).get(i), tmp);
                        result += Math.max(tmp, g.helpGuard.get(g.edges.get(in).get(i)));
                        g.helpWithout.set(in, Math.max(tmp, g.helpGuard.get(g.edges.get(in).get(i))));
                    } else if (g.helpWithout.get(g.edges.get(in).get(i)) != (int)-1 && g.helpGuard.get(g.edges.get(in).get(i)) == (int)-1) {
                        int tmp = count(problem, new Graf(g), g.edges.get(in).get(i), true);
                        g.helpGuard.set(g.edges.get(in).get(i), tmp);
                        result += Math.max(tmp, g.helpWithout.get(g.edges.get(in).get(i)));
                        g.helpWithout.set(in, Math.max(tmp, g.helpWithout.get(g.edges.get(in).get(i))));
                    } else {
                        int tmp1 = count(problem, new Graf(g), g.edges.get(in).get(i), true);
                        int tmp2 = count(problem, new Graf(g), g.edges.get(in).get(i), false);
                        int bigger = Math.max(tmp1, tmp2);
                        result += bigger;
                        g.helpWithout.set(g.edges.get(in).get(i), tmp2);
                        g.helpGuard.set(g.edges.get(in).get(i), tmp1);
                        g.helpWithout.set(in, bigger);
                    }
                }
            }
        }
        return result;
    }

    public static long solve(TreeProblem in) {
        // TODO implement
        Graf g = new Graf();
        g.refactorEdges(in);
        g.visited.set(0, true);
        return Math.max(count(in, new Graf(g), 0, true), count(in, new Graf(g), 0, false));
    }

    public static void main(String[] args) {
        assert solve( new TreeProblem( 1, Arrays.asList( new Pair( 0, 3 ), new Pair( 1,3 ), new Pair(2, 3) ), Arrays.asList(1, 1, 1, 2)) ) == 3;
        assert solve( new TreeProblem( 1, Arrays.asList( new Pair( 0, 3 ), new Pair( 1,3 ), new Pair(2, 3) ), Arrays.asList(1, 1, 1, 4)) ) == 4;
        assert  solve( new TreeProblem( 1, Arrays.asList( new Pair(1, 4), new Pair(6, 1), new Pair( 2, 1), new Pair(3, 8), new Pair(8, 0), new Pair(6, 0), new Pair(5, 6), new Pair(7, 2), new Pair(0, 9) ), Arrays.asList(17, 11, 5, 13, 8, 12, 7, 4, 2, 8)) ) == 57;
        assert  solve( new TreeProblem( 1, Arrays.asList(  new Pair (9, 7), new Pair (9, 6), new Pair  (10, 4), new Pair(4, 9), new Pair (7, 1),  new Pair(0, 2), new Pair(9, 2), new Pair(3, 8), new Pair(2, 3), new Pair(5, 4) ), Arrays.asList(10, 16, 13, 4, 19, 8, 18, 17, 18, 19, 10)) ) == 85;

    }

}
